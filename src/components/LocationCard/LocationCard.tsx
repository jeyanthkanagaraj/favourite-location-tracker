import React from 'react'
import { withStyles, Card, CardContent, Typography } from '@material-ui/core'
import { Styles } from './styles'

interface MyValue {
  name: string
  address: string
  coordinates: any
}

interface MyProps {
  classes: any
  location: MyValue
  handleSelectedPlace: Function
}

const LocationCard: React.FC<MyProps> = ({ classes, location, handleSelectedPlace }) => {
  const { name, address, coordinates } = location
  return (
    <Card className={classes.card} elevation={3} onClick={() => handleSelectedPlace(location)}>
      <CardContent>
        <Typography className={classes.cardTitle}>{name}</Typography>
        <Typography className={classes.details}>
          <span>Address</span> {address}
        </Typography>
        <Typography className={classes.details}>
          <span>Latitude</span> {coordinates[0]}
        </Typography>
        <Typography className={classes.details}>
          <span>Longitude</span> {coordinates[1]}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default withStyles(Styles)(LocationCard)
