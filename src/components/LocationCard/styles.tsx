import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  card: {
    minHeight: 300,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center' as const,
    cursor: 'pointer',
  },
  cardTitle: {
    color: theme.palette.primary.main,
    fontSize: 30,
    textTransform: 'capitalize' as const,
    fontWeight: 600,
  },
  details: {
    '& span': {
      color: theme.palette.primary.contrastText,
      display: 'block',
    },
  },
})
