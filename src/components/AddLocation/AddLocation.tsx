import React from 'react'
import { withStyles, Typography, TextField, Button } from '@material-ui/core'
import { Styles } from './styles'
import { AddLocation as Location, Close } from '@material-ui/icons'
import AutoCompletePlaces from '../AutoCompletePlaces/AutoCompletePlaces'

interface MyProps {
  classes: any
  name: string
  address: string
  handleInputChange: any
  onSubmit: any
  handleModalClose: any
  handlePlaceSelect: Function
  handlePlaceChange: Function
  isSubmitted: boolean
}

const AddLocation: React.FC<MyProps> = ({
  classes,
  name,
  address,
  handleInputChange,
  onSubmit,
  handleModalClose,
  handlePlaceSelect,
  handlePlaceChange,
  isSubmitted,
}) => {
  return (
    <div className={classes.root}>
      <Close onClick={handleModalClose} className={classes.close} />
      <Typography className={classes.title} variant="h4">
        Add Your Favourite Location <Location className={classes.icon} />
      </Typography>
      <form onSubmit={onSubmit}>
        <TextField
          type="text"
          value={name}
          onChange={handleInputChange}
          name="name"
          label="Name"
          fullWidth
          helperText={isSubmitted && !name && 'Name is required'}
          FormHelperTextProps={{
            classes: {
              root: classes.error,
            },
          }}
        />
        <AutoCompletePlaces handlePlaceChange={handlePlaceChange} handlePlaceSelect={handlePlaceSelect} address={address} />
        {isSubmitted && !address && <Typography className={classes.errorAddress}>Address is Required</Typography>}
        <Button type="submit" className={classes.button}>
          Add Location
        </Button>
      </form>
    </div>
  )
}

export default withStyles(Styles)(AddLocation)
