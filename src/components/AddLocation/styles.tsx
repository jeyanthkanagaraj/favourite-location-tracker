import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  root: {
    padding: 30,
    textAlign: 'center' as const,
    position: 'relative' as const,
  },
  close: {
    position: 'absolute' as const,
    top: 10,
    right: 10,
    color: theme.palette.primary.contrastText,
    cursor: 'pointer' as const,
  },
  title: {
    color: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
      fontSize: 15,
    },
  },
  icon: {
    fontSize: 30,
    [theme.breakpoints.down('sm')]: {
      fontSize: 20,
    },
  },
  button: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.light,
    marginTop: 20,
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
      color: theme.palette.primary.main,
      border: '1px solid',
    },
  },
  error: {
    color: 'red',
  },
  errorAddress: {
    textAlign: 'left' as const,
    fontSize: 11,
    color: 'red' as const,
  },
})
