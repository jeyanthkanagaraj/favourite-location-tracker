import React from 'react'
import PlacesAutocomplete from 'react-places-autocomplete'
import { withStyles, TextField, Typography } from '@material-ui/core'
import { Styles } from './styles'

interface MyProps {
  classes: any
  address: string
  handlePlaceChange: Function
  handlePlaceSelect: Function
}

const AutoCompletePlaces: React.FC<MyProps> = ({ classes, address, handlePlaceChange, handlePlaceSelect }) => {
  return (
    <PlacesAutocomplete value={address} onChange={handlePlaceChange} onSelect={handlePlaceSelect}>
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div>
          <TextField
            {...getInputProps({
              label: 'Address',
            })}
            fullWidth
          />
          <div className={classes.result}>
            {loading && <Typography className={classes.loader}>Loading...</Typography>}
            {suggestions.length > 0 ? (
              <>
                {suggestions.map((suggestion) => {
                  const style = suggestion.active
                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                    : { backgroundColor: '#ffffff', cursor: 'pointer' }
                  return (
                    <div
                      {...getSuggestionItemProps(suggestion, {
                        style,
                      })}
                    >
                      <Typography color="primary" className={classes.suggestion}>
                        {suggestion.description}
                      </Typography>
                    </div>
                  )
                })}
              </>
            ) : null}
          </div>
        </div>
      )}
    </PlacesAutocomplete>
  )
}

export default withStyles(Styles)(AutoCompletePlaces)
