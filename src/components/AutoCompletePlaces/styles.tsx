import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  loader: {
    color: theme.palette.primary.dark,
    fontSize: 15,
    fontFamily: 'inherit',
  },
  suggestion: {
    color: theme.palette.primary.main,
    fontSize: 15,
    fontFamily: 'inherit',
  },
  result: {
    position: 'absolute' as const,
    backgroundColor: '#fff !important',
    zIndex: 999,
    width: '80%',
  },
})
