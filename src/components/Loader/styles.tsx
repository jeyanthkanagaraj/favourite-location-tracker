import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  root: {
    margin: 0,
    padding: 0,
    position: 'fixed' as const,
    right: 0,
    top: 0,
    width: '100%',
    zIndex: 9999,
    height: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },

  loader: {
    position: 'absolute' as const,
    top: '50%',
    left: '50%',
  },
})
