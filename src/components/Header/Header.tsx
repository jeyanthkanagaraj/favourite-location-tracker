import React, { Component } from 'react'
import { withStyles, AppBar, Toolbar, IconButton, Typography, Menu, MenuItem } from '@material-ui/core'
import { Menu as MenuIcon, AccountCircle } from '@material-ui/icons'
import { Styles } from './styles'
import { logOut } from '../../store/login/action'
import { connect } from 'react-redux'

type MyProps = {
  classes: any
  handleDrawerToggle: any
  logOut: Function
}

type MyState = {
  AnchorEl: null | HTMLElement
}

class Header extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props)
    this.state = {
      AnchorEl: null,
    }
  }

  handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    this.setState({ AnchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ AnchorEl: null })
    this.props.logOut()
  }
  render() {
    const { classes, handleDrawerToggle } = this.props
    const { AnchorEl } = this.state
    return (
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="open drawer" onClick={handleDrawerToggle}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Favourite Location Tracker
          </Typography>
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={this.handleMenu}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={AnchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(AnchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={this.handleClose}>Logout</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  logOut: () => dispatch(logOut()),
})

export default connect(null, mapDispatchToProps)(withStyles(Styles)(Header))
