import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'
import { LocationOn } from '@material-ui/icons'

const CurrentLocation = (lat, lng) => (
  <div>
    <LocationOn style={{ color: 'red' }} />
  </div>
)

const FavouriteLocations = (lat, lng) => (
  <div>
    <LocationOn style={{ color: 'blue' }} />
  </div>
)

interface coordinates {
  Latitude: null | number
  Longitude: null | number
}

type MyProps = {
  center: any
  zoom: number
  currentLocation: coordinates
  locations: any
  selectedLocation: any
}

class GoogleMapLocation extends Component<MyProps> {
  static defaultProps = {
    center: {
      lat: 28.5272803,
      lng: 77.0689,
    },
    zoom: 4,
  }

  render() {
    const { currentLocation, locations, selectedLocation } = this.props
    return (
      <div style={{ height: '60vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCGoiWoR-tc0BFDwswogCsUdccEz3dBXX8' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <CurrentLocation lat={currentLocation.Latitude} lng={currentLocation.Longitude} />
          {selectedLocation ? (
            <FavouriteLocations lat={selectedLocation.coordinates[0]} lng={selectedLocation.coordinates[1]} />
          ) : (
            locations && locations.map((location) => <FavouriteLocations lat={location.coordinates[0]} lng={location.coordinates[1]} />)
          )}
        </GoogleMapReact>
      </div>
    )
  }
}

export default GoogleMapLocation
