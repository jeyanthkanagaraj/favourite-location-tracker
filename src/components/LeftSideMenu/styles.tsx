import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  toolbar: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.light,
  },
})
