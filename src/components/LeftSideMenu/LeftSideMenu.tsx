import React from 'react'
import { withStyles, List, ListItem, ListItemText, ListItemIcon, Toolbar, Typography } from '@material-ui/core'
import { Dashboard } from '@material-ui/icons'
import { Styles } from './styles'

interface MyProps {
  classes: any
  firstName: string
}

const MENU = [{ id: 0, name: 'Dashboard', path: '/dashboard' }]

function ListItemLink(props: any) {
  return <ListItem button component="a" {...props} />
}

const LeftSideMenu: React.FC<MyProps> = ({ classes, firstName }) => {
  return (
    <>
      <Toolbar className={classes.toolbar}>
        <Typography>Welcome {firstName}</Typography>
      </Toolbar>
      <List>
        {MENU.map((item) => (
          <ListItemLink button key={item.id} href={item.path}>
            <ListItemIcon>{<Dashboard />}</ListItemIcon>
            <ListItemText primary={item.name} />
          </ListItemLink>
        ))}
      </List>
    </>
  )
}

export default withStyles(Styles)(LeftSideMenu)
