import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  container: {
    background: theme.palette.primary.dark,
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column' as const,
  },
  pageTitle: {
    color: theme.palette.primary.light,
    marginBottom: 30,
    textAlign: 'center' as const,
  },
  card: {
    backgroundColor: theme.palette.primary.light,
    textAlign: 'center' as const,
    padding: 20,
    width: '35%',
    [theme.breakpoints.down('sm')]: {
      width: '80%',
    },
  },
  error: {
    color: 'red',
  },
  messageError: {
    color: 'red',
    fontSize: 13,
    marginTop: 12,
  },
  button: {
    width: '50%',
    marginTop: 30,
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.light,
  },
  title: {
    marginBottom: 20,
    color: theme.palette.primary.main,
  },
  fieldWrapper: {
    display: 'flex',
    alignItems: 'center',
    '& div': {
      '&:nth-of-type(1)': {
        marginRight: 20,
      },
    },
  },
  login: {
    marginTop: 20,
    '& a': {
      color: theme.palette.primary.main,
      textDecoration: 'none',
    },
  },
})
