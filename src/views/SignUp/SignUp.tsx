import React, { Component, ChangeEvent } from 'react'
import { withStyles, Card, CardContent, Typography, TextField, Button } from '@material-ui/core'
import { Styles } from './styles'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { registerUser } from '../../store/register/action'
import Loader from '../../components/Loader/Loader'
import { passwordValidation } from '../../utils'

type MyState = {
  FirstName: string
  LastName: string
  Email: string
  Password: string
  ConfirmPassword: string
  IsSubmitted: boolean
  [key: string]: any
}

type MyProps = {
  classes: any
  Loading: boolean
  registerUser: Function
  Message: string
}

class SignUp extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props)
    this.state = {
      FirstName: '',
      LastName: '',
      Email: '',
      Password: '',
      ConfirmPassword: '',
      IsSubmitted: false,
    }
  }

  handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    this.setState({
      [name]: value,
    })
  }

  handleSubmit = () => {
    this.setState({
      IsSubmitted: true,
    })
    let passVal = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.{8,})/
    const { FirstName, LastName, Email, Password, ConfirmPassword } = this.state
    if (FirstName && LastName && Email && passVal.test(Password) && passVal.test(ConfirmPassword) && Password === ConfirmPassword) {
      this.props.registerUser({ FirstName, LastName, Email, Password })
    }
  }

  render() {
    const { classes, Loading, Message } = this.props
    const { FirstName, LastName, Email, Password, ConfirmPassword, IsSubmitted } = this.state
    return (
      <div className={classes.container}>
        {Loading && <Loader />}
        <Typography variant="h4" className={classes.pageTitle}>
          Favourite Location Tracker
        </Typography>
        <Card className={classes.card}>
          <CardContent>
            <Typography variant="h5" className={classes.title}>
              Create New Account
            </Typography>
            <div className={classes.fieldWrapper}>
              <TextField
                type="text"
                value={FirstName}
                onChange={this.handleChange}
                name="FirstName"
                label="First Name"
                fullWidth
                helperText={IsSubmitted && !FirstName && 'FirstName is required'}
                FormHelperTextProps={{
                  classes: {
                    root: classes.error,
                  },
                }}
                required
              />
              <TextField
                type="text"
                value={LastName}
                onChange={this.handleChange}
                name="LastName"
                label="Last Name"
                fullWidth
                helperText={IsSubmitted && !LastName && 'LastName is required'}
                FormHelperTextProps={{
                  classes: {
                    root: classes.error,
                  },
                }}
                required
              />
            </div>
            <TextField
              type="email"
              value={Email}
              onChange={this.handleChange}
              name="Email"
              label="Email"
              fullWidth
              helperText={IsSubmitted && !Email && 'Email is required'}
              FormHelperTextProps={{
                classes: {
                  root: classes.error,
                },
              }}
              required
            />
            <TextField
              type="password"
              value={Password}
              onChange={this.handleChange}
              name="Password"
              label="Password"
              fullWidth
              helperText={IsSubmitted && !Password && 'Password is required'}
              FormHelperTextProps={{
                classes: {
                  root: classes.error,
                },
              }}
              required
            />
            <TextField
              type="password"
              value={ConfirmPassword}
              onChange={this.handleChange}
              name="ConfirmPassword"
              label="Confirm Password"
              fullWidth
              helperText={IsSubmitted && !ConfirmPassword && 'Confirm Password is required'}
              FormHelperTextProps={{
                classes: {
                  root: classes.error,
                },
              }}
              required
            />
            {IsSubmitted && Password.length && ConfirmPassword.length && Password !== ConfirmPassword && (
              <Typography className={classes.messageError}>Please make sure your passwords match.</Typography>
            )}
            {IsSubmitted && !passwordValidation(Password, ConfirmPassword) && (
              <Typography className={classes.messageError}>
                Password must contain at least 8 characters, including 1 uppercase letter, 1 lowercase letter and 1 number.
              </Typography>
            )}
            <Button variant="contained" className={classes.button} onClick={this.handleSubmit}>
              Sign Up
            </Button>
            <Typography className={classes.login}>
              Have an account? <Link to="/">Login here</Link>
            </Typography>
            {Message && <Typography className={classes.messageError}>{Message}</Typography>}
          </CardContent>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  Loading: state.register.Loading,
  Message: state.register.Message,
})

const mapDispatchToProps = (dispatch: Function) => ({
  registerUser: (value: MyState) => dispatch(registerUser(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(Styles)(SignUp))
