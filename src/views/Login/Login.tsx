import React, { Component, ChangeEvent } from 'react'
import { withStyles, Card, CardContent, Typography, TextField, Button } from '@material-ui/core'
import { Styles } from './styles'
import { Link } from 'react-router-dom'
import { loginUser } from '../../store/login/action'
import { connect } from 'react-redux'
import Loader from '../../components/Loader/Loader'

type MyState = {
  Email: string
  Password: string
  IsSubmitted: boolean
  [key: string]: any
}

type MyProps = {
  [key: string]: any
  Success: boolean
  Error: string
}

type MyValue = {
  Email: string
  Password: string
}

class Login extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props)
    this.state = {
      Email: '',
      Password: '',
      IsSubmitted: false,
    }
  }

  handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    this.setState({
      [name]: value,
    })
  }

  handleSubmit = () => {
    this.setState({
      IsSubmitted: true,
    })
    const { Email, Password } = this.state
    if (Email && Password) {
      this.props.loginUser({ Email, Password })
    }
  }

  render() {
    const { classes, Loading, Error, Success } = this.props
    const { Email, Password, IsSubmitted } = this.state
    return (
      <div className={classes.container}>
        {Loading && <Loader />}
        <Typography variant="h4" className={classes.pageTitle}>
          Favourite Location Tracker
        </Typography>
        <Card className={classes.card}>
          <CardContent>
            {Success && (
              <Typography variant="h5" className={classes.success}>
                Registration Successful. Please login
              </Typography>
            )}
            <Typography variant="h5" className={classes.title}>
              Login to your Account
            </Typography>
            <TextField
              type="email"
              value={Email}
              onChange={this.handleChange}
              name="Email"
              label="Email"
              fullWidth
              required
              helperText={IsSubmitted && !Email && 'Email is required'}
              FormHelperTextProps={{
                classes: {
                  root: classes.error,
                },
              }}
            />
            <TextField
              type="password"
              value={Password}
              onChange={this.handleChange}
              name="Password"
              label="Password"
              fullWidth
              required
              helperText={IsSubmitted && !Password && 'Password is required'}
              FormHelperTextProps={{
                classes: {
                  root: classes.error,
                },
              }}
            />
            <Button variant="contained" className={classes.button} onClick={this.handleSubmit}>
              Login
            </Button>
            {Error && <Typography>Please check your email and password are correct</Typography>}
            <Typography className={classes.signup}>
              Don't have an account? <Link to="/signup">Register here</Link>
            </Typography>
          </CardContent>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  Loading: state.login.Loading,
  Error: state.login.Error,
  Success: state.register.Success,
})

const mapDispatchToProps = (dispatch: Function) => ({
  loginUser: (value: MyValue) => dispatch(loginUser(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(Styles)(Login))
