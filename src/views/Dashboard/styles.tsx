import { Theme } from '@material-ui/core/styles/createMuiTheme'

const drawerWidth = 240

export const Styles = (theme: Theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },

  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  pageTitleCover: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addButton: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.light,
  },
  paper: {
    overflowY: 'inherit' as const,
  },
  locationTitle: {
    fontSize: 30,
    fontWeight: 600,
    color: theme.palette.primary.main,
    textAlign: 'center' as const,
    marginBottom: 20,
  },
  distanceDetails: {
    fontSize: 20,
    textAlign: 'center' as const,
    '& span': {
      color: theme.palette.primary.main,
    },
  },
  markerFlex: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },

  iconRed: {
    color: 'red',
  },

  iconBlue: {
    color: 'blue',
  },
})
