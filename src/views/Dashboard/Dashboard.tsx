import React, { Component, ChangeEvent } from 'react'
import { withStyles, Typography, Button, Dialog, Hidden, Drawer, Grid } from '@material-ui/core'
import { Styles } from './styles'
import Header from '../../components/Header/Header'
import AddLocation from '../../components/AddLocation/AddLocation'
import { connect } from 'react-redux'
import { addFavouriteLocation, getAllLocations } from '../../store/locations/action'
import { currentUserDetails } from '../../store/login/action'
import LeftSideMenu from '../../components/LeftSideMenu/LeftSideMenu'
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import LocationCard from '../../components/LocationCard/LocationCard'
import GoogleMapLocation from '../../components/GoogleMapLocation/GoogleMapLocation'
import { locationDistance } from '../../utils'
import { LocationOn } from '@material-ui/icons'
import Loader from '../../components/Loader/Loader'

interface location {
  Name: string
  Address: string
  Latitude: null | number
  Longitude: null | number
}

interface selectedLocation {
  name: string
  address: string
  coordinates: any
}

interface coordinates {
  Latitude: null | number
  Longitude: null | number
}

interface Props {
  window?: () => Window
}

type MyState = {
  IsModalOpen: boolean
  Location: location
  MobileOpen: boolean
  IsGeocoding: boolean
  Latitude: null | number
  Longitude: null | number
  SelectedLocation: null | selectedLocation
  IsSubmitted: boolean
}

type MyProps = {
  [key: string]: any
  Loading: boolean
  AddLoading: boolean
}

class Dashboard extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props)
    this.state = {
      IsModalOpen: false,
      MobileOpen: false,
      Location: {
        Name: '',
        Address: '',
        Latitude: null,
        Longitude: null,
      },
      Latitude: null,
      Longitude: null,
      IsGeocoding: false,
      SelectedLocation: null,
      IsSubmitted: false,
    }
  }

  componentDidMount() {
    this.props.getAllLocations()
    this.getCurrentLocation()
    this.props.currentUserDetails()
  }

  //To get current location
  getCurrentLocation() {
    const location = window.navigator && window.navigator.geolocation
    if (location) {
      location.getCurrentPosition(
        (position) => {
          this.setState({
            Latitude: position.coords.latitude,
            Longitude: position.coords.longitude,
          })
        },
        (error) => {
          this.setState({ Latitude: 0, Longitude: 0 })
        }
      )
    }
  }

  //To handle add location modal
  handleModalOpen = () => {
    this.setState({
      IsModalOpen: true,
    })
  }

  //To handle add location modal
  handleModalClose = () => {
    this.setState({
      IsModalOpen: false,
    })
  }

  //To handle google auto complete place
  handlePlaceChange = (Address: string) => {
    let Location = this.state.Location
    Location['Address'] = Address
    Location['Latitude'] = null
    Location['Longitude'] = null
    this.setState({
      Location,
    })
  }

  //To handle google auto complete place
  handlePlaceSelect = (selected: string) => {
    let Location = this.state.Location
    Location['Address'] = selected
    this.setState({ IsGeocoding: true, Location })
    geocodeByAddress(selected)
      .then((results) => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        let Location = this.state.Location
        Location['Latitude'] = lat
        Location['Longitude'] = lng
        this.setState({
          Location,
          IsGeocoding: false,
        })
      })
      .catch((error) => console.error('Error', error))
  }

  handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const Location = this.state.Location
    Location['Name'] = e.target.value
    this.setState({
      Location,
    })
  }

  //To handle Left side menu
  handleDrawerToggle = () => {
    this.setState({
      MobileOpen: !this.state.MobileOpen,
    })
  }

  //To handle the add location submission
  handleSubmit = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    this.setState({
      IsSubmitted: true,
    })
    const { Location } = this.state
    if (Location.Name && Location.Address && Location.Latitude && Location.Longitude) {
      this.props.addFavouriteLocation(this.state.Location)
      setTimeout(() => {
        if (this.props.AddSuccess) {
          this.handleModalClose()
          this.setState({
            Location: { Name: '', Address: '', Latitude: null, Longitude: null },
            IsSubmitted: false,
          })
        }
      }, 1000)
    }
  }

  //To handle the selected fav location
  handleSelectedPlace = (value: any) => {
    this.setState({
      SelectedLocation: value,
    })
  }

  render() {
    const { IsModalOpen, MobileOpen, Latitude, Longitude, SelectedLocation, IsSubmitted } = this.state
    const { Name, Address } = this.state.Location
    const { classes, window, Locations, UserDetails, Loading, AddLoading } = this.props
    const container = window !== undefined ? () => window().document.body : undefined

    return (
      <div className={classes.root}>
        {Loading || AddLoading ? <Loader /> : null}
        <Header handleDrawerToggle={this.handleDrawerToggle} />
        <nav className={classes.drawer} aria-label="mailbox folders">
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor="left"
              open={MobileOpen}
              onClose={this.handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true,
              }}
            >
              <LeftSideMenu firstName={UserDetails && UserDetails.firstName} />
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              <LeftSideMenu firstName={UserDetails && UserDetails.firstName} />
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <div className={classes.pageTitleCover}>
            <Typography variant="h5">Dashboard</Typography>
            <Button onClick={this.handleModalOpen} className={classes.addButton} variant="contained">
              Add Location
            </Button>
          </div>
          <Typography className={classes.locationTitle}>Your Favourite Locations</Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Grid container spacing={2}>
                {Locations && Locations.length > 0 ? (
                  Locations.map((location) => (
                    <Grid item xs={12} sm={12} md={12} lg={6}>
                      <LocationCard key={location.id} location={location} handleSelectedPlace={this.handleSelectedPlace} />
                    </Grid>
                  ))
                ) : (
                  <Typography>You don't have any Favourite Location. Please add some</Typography>
                )}
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              {SelectedLocation !== null && (
                <Typography className={classes.distanceDetails}>
                  Distance between your Current and Selected Favourite locations:{' '}
                  <span>{locationDistance(Latitude, Longitude, SelectedLocation.coordinates[0], SelectedLocation.coordinates[1], 'K')} KMs</span>
                </Typography>
              )}
              <div className={classes.markerFlex}>
                <Typography>
                  Your Location
                  <LocationOn className={classes.iconRed} />
                </Typography>
                <Typography>
                  Favourite Location
                  <LocationOn className={classes.iconBlue} />
                </Typography>
              </div>
              <GoogleMapLocation currentLocation={{ Latitude, Longitude }} locations={Locations} selectedLocation={SelectedLocation} />
            </Grid>
          </Grid>
          <Dialog open={IsModalOpen} onClose={this.handleModalClose} aria-labelledby="form-dialog-title" classes={{ paper: classes.paper }}>
            <AddLocation
              name={Name}
              address={Address}
              isSubmitted={IsSubmitted}
              handleInputChange={this.handleInputChange}
              onSubmit={this.handleSubmit}
              handleModalClose={this.handleModalClose}
              handlePlaceChange={this.handlePlaceChange}
              handlePlaceSelect={this.handlePlaceSelect}
            />
          </Dialog>
        </main>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  AddSuccess: state.locations.AddSuccess,
  Locations: state.locations.Locations,
  UserDetails: state.login.UserDetails,
  Loading: state.locations.Loading,
  AddLoading: state.locations.AddLoading,
})

const mapDispatchToProps = (dispatch) => ({
  addFavouriteLocation: (location) => dispatch(addFavouriteLocation(location)),
  getAllLocations: () => dispatch(getAllLocations()),
  currentUserDetails: () => dispatch(currentUserDetails()),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(Styles)(Dashboard))
