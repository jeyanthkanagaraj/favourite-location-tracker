import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#396afc',
      light: '#fff',
      dark: 'linear-gradient(to right, #396afc, #2948ff)',
      contrastText: '#ccc',
    },
  },
})
