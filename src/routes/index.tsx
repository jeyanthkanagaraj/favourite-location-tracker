import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import history from '../constants/history'
import SignUp from '../views/SignUp/SignUp'
import LogIn from '../views/Login/Login'
import Dashboard from '../views/Dashboard/Dashboard'
import PrivateRoute from './privateRoute'

class Routes extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" component={LogIn} exact />
          <Route path="/signup" component={SignUp} exact />
          <PrivateRoute path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    )
  }
}

export default Routes
