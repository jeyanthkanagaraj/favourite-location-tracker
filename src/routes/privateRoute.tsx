import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

const PrivateRoute = ({ component: Component, UID, ...rest }) => {
  console.log(UID)
  return (
    <Route {...rest} render={(props) => (UID ? <Component {...props} /> : <Redirect to={{ pathname: '/', state: { from: props.location } }} />)} />
  )
}

const mapStateToProps = (state) => ({
  UID: state.login.UID,
})

export default connect(mapStateToProps)(PrivateRoute)
