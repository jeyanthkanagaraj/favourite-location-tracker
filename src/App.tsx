import React from 'react'
import './App.css'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { theme } from './constants/theme'
import Routes from './routes'

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Routes />
    </MuiThemeProvider>
  )
}

export default App
