import {
  LOGIN_PENDING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_PENDING,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  GET_USER_DETAILS_PENDING,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_ERROR,
} from './types'
import initialState from './initialState'

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_PENDING:
      return {
        ...state,
        Loading: true,
        Success: false,
        Error: false,
      }

    case LOGIN_SUCCESS:
      console.log(action.data.user)
      return {
        ...state,
        Loading: false,
        Success: true,
        Error: false,
        UID: action.data.user.uid,
      }

    case LOGIN_ERROR:
      return {
        ...state,
        Loading: false,
        Success: false,
        Error: true,
      }

    case GET_USER_DETAILS_PENDING:
      return {
        ...state,
        UserLoading: true,
        UserSuccess: false,
        UserError: false,
      }

    case GET_USER_DETAILS_SUCCESS:
      return {
        ...state,
        UserLoading: false,
        UserSuccess: true,
        UserError: false,
        UserDetails: action.data,
      }

    case GET_USER_DETAILS_ERROR:
      return {
        ...state,
        UserLoading: false,
        UserSuccess: false,
        UserError: true,
      }

    case LOGOUT_PENDING:
      return {
        ...state,
        LogoutLoading: true,
        LogoutSuccess: false,
        LogoutError: false,
      }

    case LOGOUT_SUCCESS:
      return {
        ...state,
        LogoutLoading: false,
        UID: null,
        LogoutSuccess: true,
        LogoutError: false,
      }

    case LOGOUT_ERROR:
      return {
        ...state,
        LogoutLoading: false,
        LogoutSuccess: false,
        LogoutError: true,
      }

    default:
      return state
  }
}

export default reducer
