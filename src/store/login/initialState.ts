interface MyState {
  Loading: boolean
  Success: boolean
  Error: boolean
  UserDetails: any
  UID: null | string
  LogoutLoading: boolean
  LogoutSuccess: boolean
  LogoutError: boolean
}

const initialState = {
  Loading: false,
  Success: false,
  Error: false,
  UserDetails: null,
  UID: null,
  LogoutLoading: false,
  LogoutSuccess: false,
  LogoutError: false,
}

export default initialState
