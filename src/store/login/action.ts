import {
  LOGIN_PENDING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  GET_USER_DETAILS_PENDING,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_ERROR,
  LOGOUT_PENDING,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
} from './types'
import history from '../../constants/history'
import firebase from '../../firebase/config'

type MyResp = {
  type: string
  data?: any
  err?: any
  msg?: any
}

type Value = {
  Email: string
  Password: string
}

const startLogin = () => {
  return {
    type: LOGIN_PENDING,
  }
}

const loginCompleted = (data: MyResp) => {
  return {
    type: LOGIN_SUCCESS,
    data,
  }
}

const loginError = (err: MyResp) => {
  return {
    type: LOGIN_ERROR,
    err,
  }
}

const getUserDetails = () => {
  return {
    type: GET_USER_DETAILS_PENDING,
  }
}

const getUserDetailsCompleted = (data: MyResp) => {
  return {
    type: GET_USER_DETAILS_SUCCESS,
    data,
  }
}

const getUserDetailsError = () => {
  return {
    type: GET_USER_DETAILS_ERROR,
  }
}

const startLogout = () => {
  return {
    type: LOGOUT_PENDING,
  }
}

const logoutCompleted = () => {
  return {
    type: LOGOUT_SUCCESS,
  }
}

const logoutError = () => {
  return {
    type: LOGOUT_ERROR,
  }
}

export const loginUser = (value: Value) => {
  return (dispatch) => {
    dispatch(startLogin())

    firebase
      .auth()
      .signInWithEmailAndPassword(value.Email, value.Password)
      .then((res: any) => {
        console.log(res)
        dispatch(loginCompleted(res))
        history.push('/dashboard')
      })
      .catch((err) => {
        dispatch(loginError(err))
      })
  }
}

export const currentUserDetails = () => {
  return (dispatch) => {
    dispatch(getUserDetails())
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user)
        return firebase
          .firestore()
          .collection('users')
          .doc(user.uid)
          .get()
          .then((res: any) => {
            let data = res.data()
            dispatch(getUserDetailsCompleted({ ...user, ...data }))
          })
      } else {
        dispatch(getUserDetailsError())
      }
    })
  }
}

export const logOut = () => {
  return (dispatch) => {
    dispatch(startLogout())
    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch(logoutCompleted())
        localStorage.clear()
      })
      .catch((error) => {
        dispatch(logoutError())
      })
  }
}
