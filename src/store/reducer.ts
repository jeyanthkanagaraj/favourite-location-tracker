import { combineReducers } from 'redux'
import LoginReducer from './login/reducer'
import LocationReducer from './locations/reducer'
import RegisterReducer from './register/reducer'

const reducer = combineReducers({
  login: LoginReducer,
  locations: LocationReducer,
  register: RegisterReducer,
})
export default reducer
