import { REGISTER_PENDING, REGISTER_SUCCESS, REGISTER_ERROR } from './types'
import initialState from './initialState'

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_PENDING:
      return {
        ...state,
        Loading: true,
        Success: false,
        Error: false,
      }

    case REGISTER_SUCCESS:
      return {
        ...state,
        Loading: false,
        Success: true,
        Error: false,
      }

    case REGISTER_ERROR:
      return {
        ...state,
        Loading: false,
        Success: false,
        Error: true,
        Message: action.err,
      }

    default:
      return state
  }
}

export default reducer
