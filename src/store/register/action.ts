import { REGISTER_PENDING, REGISTER_SUCCESS, REGISTER_ERROR } from './types'
import history from '../../constants/history'
import firebase from '../../firebase/config'

type MyValue = {
  FirstName: string
  LastName: string
  Email: string
  Password: string
}

const startRegister = () => {
  return {
    type: REGISTER_PENDING,
  }
}

const registerCompleted = () => {
  return {
    type: REGISTER_SUCCESS,
  }
}

const registerError = (err: string) => {
  return {
    type: REGISTER_ERROR,
    err,
  }
}

export const registerUser = (value: MyValue) => {
  console.log(value)
  return (dispatch) => {
    dispatch(startRegister())

    firebase
      .auth()
      .createUserWithEmailAndPassword(value.Email, value.Password)
      .then((res) => {
        return firebase
          .firestore()
          .collection('users')
          .doc(res.user.uid)
          .set({
            firstName: value.FirstName,
            lastName: value.LastName,
          })
          .then(() => {
            history.push('/')
            dispatch(registerCompleted())
          })
      })
      .catch((err: any) => {
        dispatch(registerError(err.message))
      })
  }
}
