interface MyState {
  Loading: boolean
  Success: boolean
  Error: boolean
  Message: string
}

const initialState = {
  Loading: false,
  Success: false,
  Error: false,
  Message: '',
}

export default initialState
