import {
  GET_LOCATIONS_PENDING,
  GET_LOCATIONS_SUCCESS,
  GET_LOCATIONS_ERROR,
  ADD_LOCATION_PENDING,
  ADD_LOCATION_SUCCESS,
  Add_LOCATION_ERROR,
} from './types'
import initialState from './initialState'

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOCATIONS_PENDING:
      return {
        ...state,
        Loading: true,
        Success: false,
        Error: false,
      }

    case GET_LOCATIONS_SUCCESS:
      return {
        ...state,
        Loading: false,
        Success: true,
        Error: false,
        Locations: action.data,
      }

    case GET_LOCATIONS_ERROR:
      return {
        ...state,
        Loading: false,
        Success: false,
        Error: true,
      }

    case ADD_LOCATION_PENDING:
      return {
        ...state,
        AddLoading: true,
        AddSuccess: false,
        AddError: false,
      }

    case ADD_LOCATION_SUCCESS:
      let data = {
        id: action.data.id,
        name: action.data.location.Name,
        address: action.data.location.Address,
        coordinates: [action.data.location.Latitude, action.data.location.Longitude],
      }
      return {
        ...state,
        AddLoading: false,
        AddSuccess: true,
        AddError: false,
        Locations: [data, ...state.Locations],
      }

    case Add_LOCATION_ERROR:
      return {
        ...state,
        AddLoading: false,
        AddSuccess: false,
        AddError: true,
      }

    default:
      return state
  }
}

export default reducer
