import {
  GET_LOCATIONS_PENDING,
  GET_LOCATIONS_SUCCESS,
  GET_LOCATIONS_ERROR,
  ADD_LOCATION_PENDING,
  ADD_LOCATION_SUCCESS,
  Add_LOCATION_ERROR,
} from './types'
import firebase from '../../firebase/config'

type MyResp = {
  type: string
  data?: any
  err?: any
  msg?: any
}

interface MyValue {
  Name: string
  Address: string
  Latitude: string
  Longitude: string
}

const getLocations = () => {
  return {
    type: GET_LOCATIONS_PENDING,
  }
}

const locationsReceived = (data: MyResp) => {
  return {
    type: GET_LOCATIONS_SUCCESS,
    data,
  }
}

const locationError = (err: MyResp) => {
  return {
    type: GET_LOCATIONS_ERROR,
    err,
  }
}

const addLocation = () => {
  return {
    type: ADD_LOCATION_PENDING,
  }
}

const locationAdded = (data: any) => {
  return {
    type: ADD_LOCATION_SUCCESS,
    data,
  }
}

const addlocationError = (err: MyResp) => {
  return {
    type: Add_LOCATION_ERROR,
    err,
  }
}

export const getAllLocations = () => {
  return (dispatch, getState) => {
    dispatch(getLocations())
    firebase
      .firestore()
      .collection('favouriteLocations')
      .where('createdBy', '==', getState().login.UID)
      .get()
      .then((res) => {
        let data: any = res.docs.map((doc) => ({ ...doc.data(), id: doc.id }))
        dispatch(locationsReceived(data))
      })
      .catch((err) => {
        console.log(err)
        dispatch(locationError(err))
      })
  }
}

export const addFavouriteLocation = (location: MyValue) => {
  return (dispatch, getState) => {
    dispatch(addLocation())
    firebase
      .firestore()
      .collection('favouriteLocations')
      .add({
        name: location.Name,
        address: location.Address,
        coordinates: [location.Latitude, location.Longitude],
        createdBy: getState().login.UID,
      })
      .then((res) => {
        dispatch(locationAdded({ id: res.id, location: location }))
      })
      .catch((err) => {
        dispatch(addlocationError(err))
      })
  }
}
