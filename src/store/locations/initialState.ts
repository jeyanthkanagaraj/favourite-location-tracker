interface MyState {
  Loading: boolean
  Success: boolean
  Error: boolean
  Locations: []
  AddLoading: boolean
  AddSuccess: boolean
  AddError: boolean
}

const initialState = {
  Loading: false,
  Success: false,
  Error: false,
  Locations: [],
  AddLoading: false,
  AddSuccess: false,
  AddError: false,
}

export default initialState
