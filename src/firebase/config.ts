import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSENGER__ID,
  appId: process.env.APP_ID,
  measurementId: process.env.MEASURE_ID,
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)
export default firebase
