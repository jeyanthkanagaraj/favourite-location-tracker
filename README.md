# Favourite Location Tracker

Favourite Location Tracker application helps you to store your favourite location. You can pick your favourite application from google map and you can store it in your accout. You can register an account in the application and store your locations in your accont. It is safe and secure.

## Tech Stack

ReactJS, Redux, Firebase

## Prerequisite

- Node v10+
- NPM v6+

## Quick Start

To get start, UnZip the application folder and open command line and go to the project root folder and execute the below command

```
npm install or yarn install
npm start or yarn add

```

The application will start compliling, once it compiled successfully, it will automatically open your browser and the application will run in http://localhost:3000/

### User stories

- User will see the application landing page/login page.
- User can login to the account, if he already hass an account, else he can register for new account using Register here option
- Once User logged in, he will redirect to the dashboard page. Where user can view his/her favourite locations in card and map view.
- If the user is logged in for the first time, the user won't have any locations.
- User can add new location by clicking Add Location button. It will open a Modal with form create a location.
- To create a new location, user need to add location name and address.
- The address field is integrated with Google location API, which will list automatically the location details, after the user entered few character of the location name.
- Once he filed both fields, he can click the add location button, which will add the location to the card list and also to the map view
- User can see his/her current location and his/her favourite location in the map(User cuurent location will be in red marker icon and favourite locations in blue marker icon)
- By default, In the map view, User will see his/her location pin and all his/her favourite location pin.
- If the User selects any location in the map, user will see only his/her current location and the selected favourite application pin in the map with distance details between those two locations above the map.
- User can logout by clicking the User avatar on the top right corner, which will open the logout option. OnClicking the logout option, the user will be logged out and redirected to the login page.
